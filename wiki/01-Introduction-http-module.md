# Http module

Node.js has a built-in module called HTTP, which allows Node.js to transfer data over the Hyper Text Transfer Protocol (HTTP).

To include the HTTP module, use the `require()` method:

```js
var http = require("http");
```

## Http Server

The HTTP module can create a client/server connection using HTTP protocol.
### How web-server works?
![server-client](https://s3.ap-south-1.amazonaws.com/appdev.konfinity.com/Nodejs+/NodeBackEndFrontEnd_Update_1.gif)
We can create a new server using the method
```js
http.createServer();
```

This creates an object of `http.Server` class and with this object we can use two methods:

- `close()` stops the server from accepting new connections
- `listen()` starts the HTTP server and listens for connections

The method `createServer()` takes a callback function with two arguments - _request_ and _response_.

The following code snippet shows how to create and use an http server.

![](https://s3.ap-south-1.amazonaws.com/appdev.konfinity.com/httpBasicServer.png)

- In the above code, we are creating a server module which will listen on specified port no.(7000). If a request is made through the browser on this port no, the server application will send a 'Hello' World' response to the client.

Let us understand the code line by line:

- In the above code, in line 2, we are creating a server application that is called whenever a request is made to our server application.

- In line 3, when a request is received by the server, it must send a response with a header type of '200.' This number is the normal response which is sent in an HTTP header when a successful response is sent to the client. `res.WriteHead()` is called to write the header of the response, that the application will serve to the client. 

- In line 4, the server is sending the string 'Hello World.' in response to the client. `res.end` is a method which sends the string passed into it to the client and also signals to the server that the response (header and content) has been sent completely.

- In line 5, `server.listen()` is used to make the server listen to a certain specified port address.

The above code runs on the port number 7000 and produces the result as follows:

![](https://s3.ap-south-1.amazonaws.com/appdev.konfinity.com/HTTP.png)

The whole above process can be visualized using the following image:

![](https://s3.ap-south-1.amazonaws.com/appdev.konfinity.com/http_flow.png)

### server.listen(port, hostname, backlog, callback);

The server.listen() method creates a listener on the specified port or path.

- **port** Optional. Specifies the port we want to listen to
- **hostname** Optional. Specifies the IP address we want to listen to
- **backlog** Optional. Specifies the max length of the queue of pending connections. Default 511
- **callback** Optional. Specifies a function to be executed when the listener has been added

**server.close(callback);**

- callback Optional. Specifies a function to be executed after the server stops listening for connections

The server.close() method stops the HTTP server from accepting new connections.

All existing connections are kept.

## HTTP Requests

The four most common HTTP requests are _GET_, _POST_, _PUT_, _DELETE_. The type of request determines the type of operation the server should perform.

### **GET**

- `GET` requests are the most common and widely used method to send request to the server. This type of request is used to **retrieve data from a server at the specified resource**.

- `GET` request is only requesting data and does not modify any resources, therefore, it is considered a _safe_ and _idempotent_ method.

### **POST**

- `POST` requests are used to **send data to the API server to create or update a resource**.

- The most common example is a _contact form_ which takes data in its input fields and when the user clicks _send_ button, the data is sent to the server. In this type of case, we use the POST request.
- `POST` is _non-idempotent_ i.e. it updates or modifies the data.

### **PUT**

- Similar to _POST_, `PUT` requests are used to **send data to the API to create or update a resource**. The difference is that PUT requests are _idempotent_
- That is, calling the same `PUT` request **multiple times will always produce the same result**.

### **DELETE**

- The `DELETE` method is exactly as it sounds: **delete the resource at the specified URL**.