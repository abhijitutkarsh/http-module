# Routing Using HTTP

Routing refers to how an application responds to the client. It is a way of organizing and managing application states. Routing comes into play when there is a request to the server, The request may be any of the previously described requests like GET, POST, etc. 

While using HTTP, we need to handle different types of requests with separate codes. We will understand routing through a code.

Consider the following code in which we will write the contents of an HTML file as a response to the client.

Now from here onwards everything needs to be done inside **`src`** folder.

The HTML code is as follows which should be saved as `index.html`:

```html
<html>
<head>
    <meta charset="utf-8" />
    <title>The Page Returned by Making Http Call to Node.js</title>
    <style type="text/css">
        table, td {
          border:double;
        }
    </style>
</head>
<body>
    <h1>Product Information Page</h1>
    <table>
        <tr>
            <td>Product Id:</td>
            <td>
                <input type="text" />
            </td>
        </tr>
        <tr>
            <td>Product Name:</td>
            <td>
                <input type="text" />
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <input type="button"  value="Save"/>
            </td>
        </tr>
    </table>
 
 
</body>
</html>

```
The code above creates a simple table with `3` rows and `2` columns. The above page must be sent as a response to the client as the request is made to the server. Thus now let's create a server which `routes` the application to the above page.

Consider the following code and copy in a file named: `app.js`

```js
//1.   Importing section
var http = require('http');
var fs = require('fs');


//2.    Creating a server with a callback function    
var server = http.createServer(function (req, resp) {
    
    
    //3. Providing a route to send the HTML page as response
    if (req.url === "/create") {
        fs.readFile("src/index.html", function (error, pgResp) {
            if (error) {
                resp.writeHead(404);
                resp.write('Contents you are looking are Not Found');
            } else {
                resp.writeHead(200, { 'Content-Type': 'text/html' });
                resp.write(pgResp);
            }
             
            resp.end();
        });
    } else {
        
        
        //4.  If the above specified url is not found, then send the following response.
        resp.writeHead(200, { 'Content-Type': 'text/html' });
        resp.write('<h1>Welcome</h1>');
        resp.end();
    }
});


//5.  Make the server listen to the port number 5050
server.listen(5050);
 
console.log('Server Started listening on 5050');
module.exports = server
```

The above is divided into 5 sections:

- The first section is the `import` section in which we have included modules and libraries which we must be requiring.

- The second section is used to create a server as we did in the previous articles.

- The third section is the **main section** to be considered. In its first line, we have given a path or URL to send the HTML page created above as a response to the client. What this means is **whenever the server gets a request on the above-specified URL, it must send the content of `index.html` page as a response to the client.**

- In the following lines in the third section, we have simply read from a file that we have already learned in our previous modules. The main purpose here is that when there is a request at the specified URL, the file must be read and its content must be sent as a response to the client. **Also consider that we have set the content-header to be text/Html and therefore, it will show a table as response and not the <tr> or <td> tags.**

- In the fourth section, we have ensured that if the above-specified URL is not requested by the user but the server is requested on any other URL, then it must send `welcome` as the response to the client.

- In the fifth section, we have made the server listen to the port number `5050`.

Below command will start your server so that you could see it's output on browser.
```
npm run task
```

Once your server get started, you will have following output:

When the server is requested at `/create` URL:

![](https://s3.ap-south-1.amazonaws.com/appdev.konfinity.com/create.png)


When the server is requested at any other URL:

![](https://s3.ap-south-1.amazonaws.com/appdev.konfinity.com/welcome.png)

**Export your function using** 

```
module.exports = server;
````

**Run the following command before submitting your task**
```
npm run test
```