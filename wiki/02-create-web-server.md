# Creating a Web Server using Node

## `Note`:
*The below-given documentation contains articles related to the `localtunnel` which has been obsolete now from the IT industry. So skip that much part where it comes to the localtunnel installation or running that through the command line and to start the server in below command ommit the localtunnel part so the remaining part will be `node server.js`*

#### Now continue

Node.js provides an **http** module which can be used to create an HTTP client of a server.

**Note : `This is your TASK, So do follow the steps as mentioned`.**

In this project , We will learn to make a request to the server and get an HTML file as response from the server.

We will run our server on our localhost. `localtunnel` is a tool which will help us to use our localhost to be served online. We can test the application or website we have created online. In other words, we can access our localhost online. This tool provides us with a `url` which can use to access our application online.
It will provide a `url` in the following way:

![](https://s3.ap-south-1.amazonaws.com/appdev.konfinity.com/localtunnel.png)

In the above image, look at the terminal below, we have a `url` generated in then terminal which we can paste in the browser and can visualize our application created.


So first, download localtunnel on the workspace:

```js
 npm install -g localtunnel
```

## A basic HTTP server

Let’s start with the server module. Create the file **server.js** in the root directory of your project, and fill it with the following code:

```js
var http = require("http");
var server = http.createServer(function(request, response) {  //Creating the server with a callback function
    response.writeHead(200, { "Content-Type": "text/plain" });   //writing a header of response
    
    
    //It is used to send the response to the client. Whatever is written into it is directly sent as response to the client.
    response.write("Hello World"); 


    response.end();     // Signals the server that the content and the header have been completely sent to the client
  })
  server.listen(8888); //The server will listen on port number 8888.
```

The above creates a server, makes the server to listen on port number `8888` and sends a response to the client whenever a request will be made on the specified port number.

Now run this code i.e. your **`server.js`** to check whether it is working or not use below command :

```js
node server.js & lt --host http://localtunnel.konfinity.com -p 8888
```

In the above command `node server.js` is used to run the js file. But here we want to run the code on localhost using localtunnel so we add `lt -p <portNumber>` to `node app.js` using `&` symbol.

Now, open your browser and point it to the link generated at the terminal. This should display a web page that
says `“Hello World”`.

The output can be seen as follows:

![](https://s3.ap-south-1.amazonaws.com/appdev.konfinity.com/HTTP.png)

Now use `Ctrl+C` to stop the hosting. But there may be some exceptional cases where you may lose your control from the running application through your terminal and now you have to kill the process working on that port so that port can be used again.

* > `There may be some scenario like if you run your localhost server and have accidentally closed your VsCode, and now you have lost control over your server through the terminal so in that case if you want to run again the same web-application then, it will not run as the required port number had already been acquired by your previous running application. So in that case, this command will help you in finding the` **project ID** `of that application so that you could kill that process forcibly. And be able to run your application again on the same port`.

First, we have to find the process id (pid), for that use below command :

```js
ps aux | grep node
```

now it will show output like this :


![](https://s3.ap-south-1.amazonaws.com/appdev.konfinity.com/command.png)

Select the pid from the first column referring to `server.js` i.e. which is `61` in above case, and kill the process using `kill <pid>` command as shown above.

**Note: If you do not kill the process, the server will not be able to listen to the same port number for any other application unless you kill the previous one**.

## Analyzing our HTTP server

It should be clear what we are actually doing here: we need to pass a function to the `http.createServer()` function. Till now we have passes an anonymous callback function. But another way of doing the same task can be as follows:

```js
var http = require("http");

function onRequest(request, response) {
  response.writeHead(200, { "Content-Type": "text/plain" });
  response.write("Hello World");
  response.end();
}

http.createServer(onRequest).listen(8888);
```

- In the above code, we have create a function called `onRequest` which we have later passed to the `http.createServer()` function.
- Also consider that in the above code we have not stored the `http.createServer()` into a variable called `server` as we did before in the previous examples. We can directly use `.listen()` function over `createServer()` as in the code above.